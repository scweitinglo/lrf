/*
 * 作者: 羅偉庭
 * 公司: 巨普科技股份有限公司
 * 日期: 2023/12/18
 *
 * 板子:Teensy 4.1
 * CPU Speed: 1.008GHZ
 *
 * 註解:
 * 本程式由巨普科技股份有限公司開發，屬於公司機密資訊，未經許可禁止複製、修改、或散佈。
 * 本程式所包含之智慧財產權為巨普科技股份有限公司所有，第三方未經授權不得使用。
 * 如欲取得進一步資訊，請聯絡巨普科技股份有限公司。
 */

#define startPin 7  // 模擬雷射發射信號
#define stopPin 8   // 模擬雷射接收信號

void setup() {
  pinMode(startPin, OUTPUT);
  digitalWrite(startPin, LOW);
  pinMode(stopPin, OUTPUT);
  digitalWrite(stopPin, LOW);
}
void loop() {
  digitalWrite(startPin, HIGH);
  delayNanoseconds(22);
  digitalWrite(stopPin, HIGH);
  delayMicroseconds(1);
  digitalWrite(startPin, LOW);
  digitalWrite(stopPin, LOW);
  delay(1);
}
