# 雷射測距系統
 * 作者: 羅偉庭
 * 公司: 巨普科技股份有限公司
 * 日期: 2024/01/09
 *
 * 註解:
 * 本程式由巨普科技股份有限公司開發，屬於公司機密資訊，未經許可禁止複製、修改、或散佈。
 * 本程式所包含之智慧財產權為巨普科技股份有限公司所有，第三方未經授權不得使用。
 * 如欲取得進一步資訊，請聯絡巨普科技股份有限公司。
 */


## arduino 文件夾

包含lrftdc,test1,test2,以及test3文件夾。其中

雷測收發模擬器: test1.ino test2.ino test3.ino

## documents 文件夾
距離計算系統:lrftdc.ino
- LRF外包內容.pdf：需要完成和測試的內容
- TDC-GP22-Application-Note-Laser-range-finder.pdf：TDC-GP22官方文件
- TDC-GP22_LRF.pdf：TDC-GP22 LRF 官方文件

## library
根據 https://github.com/leokoppel/GP22 為基礎調整適合本專案的TDC-GP22 Arduino Library

